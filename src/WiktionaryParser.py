from src.XMLParser import XMLParser
from src.HeadingParser import HeadingParser
from src.TemplateHandler import TemplateHandler
import json
import re


class WiktionaryParser:
    def __init__(self, source_file, language):

        self.source_file = source_file
        self.language = language

        self.raw_root = None
        self.heading_root = None

        self.xml_parser = None
        self.heading_parser = None
        self.template_handler = None

        # Load language codes
        with open('../data/language_codes.json', 'r') as f:
            json_file = f.read()
        self.code_dict = json.loads(json_file)
        if language in self.code_dict:
            self.code = self.code_dict[language]
        else:
            print('Unknown language name')

    def save_raw(self, target_path, max_pages=-1):
        if not self.xml_parser:
            self.xml_parser = XMLParser(self.source_file, self.language)

        self.raw_root = self.xml_parser.extract_relevant_pages(max_pages)
        print('Found {0:d} entries in total'.format(len(self.raw_root['entries'])))

        # Create a JSON file and save
        if target_path:
            json_file = json.dumps(self.raw_root, indent=4, ensure_ascii=False)
            with open(target_path + '/' + self.language + '_raw.json', 'w') as f:
                f.write(json_file)

    def save_heading_separated(self, target_path, source_path=None, max_entries=-1):

        # If raw file is provided use it
        if source_path:
            with open(source_path, 'r') as f:
                json_file = f.read()
            self.raw_root = json.loads(json_file)
            self.heading_parser = None      # reset the HeadingParser

        # First produce the raw data
        else:
            self.save_raw(None, max_entries)

        # Load HeadingParser
        if not self.heading_parser:
            self.heading_parser = HeadingParser(self.raw_root['entries'])

        # Get the content
        self.heading_root = self.heading_parser.separate_contents_by_headings(max_entries)

        # Save as a JSON
        if target_path:
            json_file = json.dumps(self.heading_root, indent=4, ensure_ascii=False)
            with open(target_path + '/' + self.language + '_heading_separated.json', 'w') as f:
                f.write(json_file)

    def save_formatted_json(self, target_path, heading_path=None, raw_path=None, xml_path=None):

        # Load from existing file
        if heading_path:
            with open(heading_path, 'r') as f:
                json_file = f.read()
            self.heading_root = json.loads(json_file)

        # No file available, pre-process first, todo
        else:
            # No heading separated available
            if not self.heading_root:
                # Load from raw file if exists
                if raw_path:
                    self.save_heading_separated(None, raw_path)
                else:
                    if xml_path:
                        self.save_raw(None, xml_path)
                        self.save_heading_separated(None, raw_path)
                    else:
                        print('No pre-existing files nor XML file provided')
                        return

        if not self.template_handler:
            self.template_handler = TemplateHandler(self.language)

        # Root of the JSON file
        root = {'entries': []}

        # Find definitions
        for entry in self.heading_root['contents']:

            # Remove XML tags
            content = self._remove_xml_tags(entry['content'])
            definitions = (re.findall(r'#\s.*', content))

            # Clean definitions
            clean_definitions = []
            for definition in definitions:
                def_dict = self.template_handler.handle_templates(definition)
                clean_definitions.append(def_dict)

            # todo: no part of speech information
            root['entries'].append({'title': entry['title'], 'definitions': clean_definitions})


        # Save
        json_file = json.dumps(root, indent=4, ensure_ascii=False)
        with open(target_path + '/' + self.language + '_formatted.json', 'w') as f:
            f.write(json_file)

    @staticmethod
    def _remove_xml_tags(text):
        text = re.sub(r'<!--[\w\W\s\S]*?-->', '', text, re.M)
        text = re.sub(r'<ref.*?>[\w\W\s\S]*?</ref>', '', text, re.M)
        text = re.sub(r'<ref.*?/>', '', text)
        return text


if __name__ == '__main__':
    SOURCE_PATH = '/Users/hiroyoshiyamasaki/Downloads/enwiktionary-20191001-pages-meta-current.xml'
    LANGUAGE = 'French'

    wiktionary_parser = WiktionaryParser(SOURCE_PATH, LANGUAGE, CODE)
    wiktionary_parser.save_raw('raw')
