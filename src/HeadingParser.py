import re
import json
import argparse


class HeadingParser:
    """
    Used separate the text body into separate headings such as ===Adjective===, ===Noun=== etc.
    """
    def __init__(self, raw_entries):
        """
        :param raw_entries: list of entries of the form {'title': title, 'body': body}
        """
        self.raw_entries = raw_entries
        self.unknown = []

    def separate_contents_by_headings(self, max_num_contents=-1, treatment={}, level=3):

        content_list = []
        num_contents = 0
        for entry in self.raw_entries:
            title = entry['title']
            body = entry['body']

            # Find all headings, e.g. ===Noun===
            regex = r'={' + str(level) + r'}.*?={' + str(level) + r'}'  # e.g. '={3}.*?={3}'
            headings = re.findall(regex, body)

            # For each heading find corresponding content
            for heading in headings:

                # Check if the heading is relevant
                if self._treat_heading(heading, treatment):

                    # e.g. '===Heading===[\w\W\s\S]*?(?=(={3}|-{4}|$))'
                    regex = heading + r'[\w\W\s\S]*?(?=(={' + str(level) + r'}|-{4}|$))'  # fixme: why does it work?
                    content = re.search(regex, body).group()

                    content_list.append({'title': title, 'content': content})

                    num_contents += 1
                    if num_contents % 10000 == 0:
                        print('Found {0:d} heading separated entities'.format(num_contents))

            if num_contents > max_num_contents > 0:
                break

        return {'contents': content_list}

    def _treat_heading(self, heading, treatment):
        """
        Check if the heading is relevant.
        :param heading: heading of the form ===Heading===
        :param treatment:
        :return: boolean, true if relevant, false otherwise
        """

        if treatment == dict():
            treatment['pos'] = True
            treatment['variation'] = True
            treatment['affix'] = False
            treatment['phrasal'] = True
            treatment['semantic'] = False
            treatment['pronunciation'] = False
            treatment['additional'] = False
            treatment['ignore'] = False
            treatment['miscellaneous'] = True
        else:
            heading_list = ['pos', 'variation', 'affix', 'phrasal', 'semantic', 'pronunciation', 'additional',
                            'ignore', 'miscellaneous']
            for h in heading_list:
                if h not in treatment:
                    treatment[h] = False

        # Strip the heading
        heading = re.sub(r'=', '', heading)
        heading = re.sub(r'(^\s|\s$)', '', heading)

        # Part of speeches
        pos_headings = ['Adjective', 'Adverb', 'Article', 'Articles', 'Conjunction', 'Determiner', 'Interjection',
                        'Noun', 'Participle', 'Particle', 'Postposition', 'Preposition', 'Pronoun', 'Verb']

        # Indicating a variation of another lemma
        variation_headings = ['Abbreviation', 'Abbreviations', 'Acronym', 'Clipping', 'Contraction', 'Initialism']

        # Various affixes
        affix_headings = ['Affix', 'Infix', 'Interfix', 'Prefix', 'Suffix']

        # Phrasal entries
        phrasal_headings = ['Idiom', 'Idioms', 'Phrase', 'Proverb']

        # Indicates semantic relationships
        semantic_headings = ['Antonyms', 'Holonyms', 'Hypernyms', 'Hyponyms', 'Meronyms', 'Paronyms', 'Synonyms']

        # Pronunciation
        pronunciation = ['Pronunciation']

        # Additional information
        additional = ['Anagrams', 'Notes', 'Remarks']

        # Mostly useless information
        ignore = ['Citations', 'Conjugation', 'Inflection', 'Quotations', 'References']

        # Miscellaneous
        miscellaneous = ['Symbol', 'Numeral', 'Letter']

        if heading in pos_headings:
            return treatment['pos']
        elif heading in variation_headings:
            return treatment['variation']
        elif heading in affix_headings:
            return treatment['affix']
        elif heading in phrasal_headings:
            return treatment['phrasal']
        elif heading in semantic_headings:
            return treatment['semantic']
        elif heading in pronunciation:
            return treatment['pronunciation']
        elif heading in additional:
            return treatment['additional']
        elif heading in ignore:
            return treatment['ignore']
        elif heading in miscellaneous:
            return treatment['miscellaneous']
        else:
            self.unknown.append(heading)
            return False


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description='Separate text body by headings')

    # Define arguments
    arg_parser.add_argument('source_path', help='path to the JSON file')
    arg_parser.add_argument('target_path', help='path to the directory to save the JSON file in')
    arg_parser.add_argument('language', help='name of the target language')
    arg_parser.add_argument('code', help='language code for the target language')
    arg_parser.add_argument('--max_entries', help='maximum number of entries to load')

    # Get arguments
    args = arg_parser.parse_args()

    # Load JSON file
    with open(args.source_path, 'r') as f:
        json_file = f.read()
    root = json.loads(json_file)

    # Get the heading parser
    parser = HeadingParser(root['entries'])

    # If no maximum entry is specified set to -1
    max_entries = args.max_entries if args.max_entries else -1

    # Get the content
    contents = parser.separate_contents_by_headings(-1)

    # Save as a JSON
    json_file = json.dumps(root, indent=4, ensure_ascii=False)
    with open(args.target_path + '/' + args.language, 'w') as f:
        f.write(json_file)
