import xml.etree.ElementTree as etree
import re
import json
import argparse


class XMLParser:
    """
    Parses the Wiktionary XML dump and looks for the relevant pages.
    The result is returned as a dictionary that can be saved as a json file
    """
    def __init__(self, source_path, language):
        """
        :param source_path: path to the XML file
        :param language: target language, case sensitive, must be exact, e.g. 'French'
        """
        self.source_path = source_path
        self.language = '==' + language + '=='      # target language is a level 2 heading
        self.ignored = []

        # Load language codes
        with open('../data/language_codes.json', 'r') as f:
            json_file = f.read()
        self.code_dict = json.loads(json_file)
        if language in self.code_dict:
            self.code = self.code_dict[language]
        else:
            print('Unknown language name')

    def extract_relevant_pages(self, num_pages=-1):
        """
        Returns a json compatible dictionary containing relevant sections of the XML file
        :param num_pages: number of pages to load, if negative all relevant pages will be loaded
        :return: a dictionary {'entries': entry_list} where entry_list is a list of dictionaries of
        the form {'title': title, 'body': body}
        """

        entry_list = []
        for i, (title, body) in enumerate(self._get_page()):

            entry_list.append({'title': title, 'body': body})

            if i >= num_pages and not num_pages < 0:
                break

            if i % 1000 == 0:
                print('Extracted {0:d} entries'.format(i))

        entry_list.sort(key=lambda x: x['title'])
        return {'entries': entry_list}

    def _get_page(self):
        """
        A generator which returns title and body of a page
        :return: (title of the page, text body of the page)
        """

        title = ''
        body = ''
        for event, elem in etree.iterparse(self.source_path, events=('start', 'end')):
            tag_name = self._strip_tag_name(elem.tag)

            # Start events
            if event == 'start':

                # Beginning of a page
                if tag_name == 'page':
                    title = ''
                    body = ''

            # End events
            elif event == 'end':

                # title
                if tag_name == 'title':
                    title = elem.text if elem.text else ''

                # Main text body
                elif tag_name == 'text':
                    body = elem.text if elem.text else ''

                # End of the page
                elif tag_name == 'page':

                    # Ignore wiki meta data
                    if self._find_wiki_texts(title):
                        continue

                    # Find the target language
                    if self.language in body:
                        body = self._strip_page_body(body)

                        yield title, body

                    elem.clear()

    @staticmethod
    def _strip_tag_name(tag):
        """
        Strip the tag name
        :param tag: of the form {...}tag
        :return: tag, e.g. page, text, title etc.
        """
        idx = tag.rfind('}')
        if idx != -1:
            tag = tag[idx + 1:]
        return tag

    def _strip_page_body(self, body):
        """
        Strips the text body of text unrelated to the target language
        :param body: entire text body of a page
        :return: entry for the target language
        """

        # Look for ==Language== ... ----
        result = re.search(self.language + r'[\w\W\s\S]*?(----)', body)

        # Look for the ==Language== ... $ for entry at the end of the page
        if not result:
            result = re.search(self.language + r'[\w\W\s\S]*$', body)

        return result.group()

    def _find_wiki_texts(self, title):
        """
        Find irrelevant Wiki metadata
        :param title:
        :return:
        """

        # Ignore anything with '...:...' (this may include some words with : in it)
        if re.search(r'.*:.*', title):
            self.ignored.append(title)
            return True
        else:
            return False


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description='Parse Wiktionary XML dump')

    # Define arguments
    arg_parser.add_argument('source_path', help='path to the XML file')
    arg_parser.add_argument('target_path', help='path to the directory to save the JSON file in')
    arg_parser.add_argument('language', help='name of the target language')
    arg_parser.add_argument('--max_entries', help='maximum number of entries to load')

    # Get arguments
    args = arg_parser.parse_args()

    # Get the parser
    xml_parser = XMLParser(args.source_path, args.language)

    # If no maximum entry is specified set to -1
    max_entries = args.max_entries if args.max_entries else -1

    # Find the pages
    root = xml_parser.extract_relevant_pages(max_entries)
    print('Found {0:d} entries in total'.format(len(root['entries'])))

    # Create a JSON file and save
    json_file = json.dumps(root, indent=4, ensure_ascii=False)
    with open(args.target_path + '/' + args.language + '_raw.json', 'w') as f:
        f.write(json_file)