import re
import json

# todo do the final test
class TemplateHandler:
    """
    Used to strip definitions of wiktionary templates {{...}} and links [[...]]
    """

    def __init__(self, language):

        # Templates for inflectional entries
        self.morphological = ['agent noun of', 'comparative of', 'contraction of', 'diminutive of', 'gerund of',
                              'female equivalent of', 'feminine noun of', 'feminine of', 'feminine plural of',
                              'feminine plural past participle of', 'feminine singular of',
                              'feminine singular past participle of', 'infl of', 'inflection of',
                              'masculine plural of', 'masculine plural past participle of', 'past participle of',
                              'plural of', 'present participle of', 'singular of', 'superlative of', 'verbal noun of']

        # Templates for entries that point to other entries
        self.variations = ['abbr of', 'abbreviation of', 'acronym of', 'alt form', 'alt sp', 'alternative case form of',
                           'alternative form of', 'alternative form of ', 'alternative spelling of', 'altform',
                           'aphetic form of', 'apocopic form of', 'archaic form of', 'archaic spelling of',
                           'clipping of', 'clipping', 'dated form of', 'ellipsis of', 'euphemistic form of',
                           'eye dialect of', 'informal form of', 'informal spelling of', 'init of', 'initialism of',
                           'misspelling of', 'nonstandard form of', 'nonstandard spelling of', 'obsolete form of',
                           'obsolete spelling of', 'rare form of', 'rare spelling of', 'short for', 'syncopic form of',
                           'syn of', 'synonym of']

        # Templates that needs to be stripped without being removed
        self.strip = ['cln', 'indtr', 'jump', 'link', 'l', 'll', 'm', 'mention', 'm+', 'n-g', 'ngd',
                      'non-gloss definition', 'non-gloss', 'sense']

        # Templates that need to be completely removed
        self.ignore = ['ISBN', 'R:CNRTL', 'R:TLFi', 'attention', 'attn', 'cite-book', 'cite-web', 'rfdef', 'rfgloss',
                       'rfex', 'rft-sense', 'taxlink', 'w', 'ja-r']

        self.language = language

        # Load language codes
        with open('../data/language_codes.json', 'r') as f:
            json_file = f.read()
        self.code_dict = json.loads(json_file)
        if language in self.code_dict:
            self.code = self.code_dict[language]
        else:
            print('Unknown language name')

    def handle_templates(self, definition):
        """
        Clean the definition and returns a json compatible dictionary of the content
        :param definition: a line of definition of the form '# ...'
        :return: parsed definition as a dictionary. The keys are
        'definition': '', 'labels': [], 'glosses': [], 'qualifiers': [], 'accents': [], 'topics': [], 'only': [],
        'post_1990': '', 'defdate': '', 'lemma': '', 'name': '', 'args': []
        """

        # Initialize the entry information
        self._init_entry()

        # Remove links, i.e. [[links]]
        definition = self._strip_links(definition)

        # Miscellaneous stuff
        definition = re.sub(r'{{(,|\.\.\.)}}', '', definition)  # e.g. {{,}}, {{...}}
        definition = re.sub(r'#\s', '', definition)             # definition starts with #

        # Remove all templates starting with the innermost templates
        while self._is_template(definition):

            # Innermost templates
            templates = re.findall(r'{{[^{}]*\|?[^{}]*}}', definition)

            # Get the definition text without the innermost templates
            for template in templates:
                definition = self._handle_template(template, definition)

        # Final clean up
        definition = re.sub(r'(^\s*|\s*$)', '', definition)     # remove leading/trailing indents
        definition = re.sub(r'\s{2,}', ' ', definition)         # multiple spaces replaced with a single space
        definition = self._handle_html(definition)              # remove HTML codes
        definition = definition.capitalize()

        definition_entry = {'definition': definition,
                            'labels': self.labels,
                            'glosses': self.glosses,
                            'qualifiers': self.qualifiers,
                            'accents': self.accents,
                            'topics': self.topics,
                            'only': self.only,
                            'post_1990': self.post_1990,
                            'defdate': self.defdate,
                            'lemma': self.lemma,
                            'name': self.name,
                            'args': self.args}

        return definition_entry

    def _init_entry(self):
        """
        Initialize the entry content
        :return: None
        """

        self.text = ''
        self.labels = []
        self.glosses = []
        self.qualifiers = []
        self.accents = []
        self.topics = []
        self.only = []
        self.post_1990 = ''
        self.defdate = ''
        self.lemma = ''
        self.name = ''
        self.args = []

    @staticmethod
    def _is_template(definition):
        """
        Check if there is a innermost template left
        :param definition:
        :return: true if there is at least one, false else
        """

        return True if re.search(r'{{[^{}]*}}', definition) else False

    @staticmethod
    def _handle_html(definition):
        definition = re.sub(r'&mdash;', '—', definition)    # replace HTML mdash code with unicode
        definition = re.sub(r'&ndash;', '–', definition)    # replace HTML ndash code with unicode
        definition = re.sub(r'&lquo', '“', definition)      # replace HTML ldquo code with unicode
        definition = re.sub(r'&rdquo', '”', definition)     # replace HTML rdquo code with unicode
        return definition

    def _handle_template(self, template, definition):
        """
        'Switchboard' method. Calls appropriate method that can handle the given template
        :param template: template to be handled
        :param definition: original definition text
        :return: new definition text
        """

        # Get the name of the template, e.g. 'label'
        template_name = self._get_name(template)

        # Call appropriate method
        if template_name in self.morphological:
            text, lemma, name, args = self._handle_morphology(template, template_name)
            self.lemma += lemma
            self.name += name
            self.args.extend(args)

        elif template_name in self.variations:
            text, lemma = self._handle_variations(template)
            self.lemma += lemma

        elif template_name in self.strip:
            text, glosses = self._handle_strip(template, template_name)
            self.glosses.extend(glosses)

        elif template_name in self.ignore:
            text = ''

        elif template_name == '&lit':
            text = self._handle_lit(template)

        elif template_name == 'IPAchar':
            text = self._handle_ipa(template)

        elif template_name == 'Latn-def':
            text = self._handle_latn_def(template)

        elif template_name == 'vern':
            text = self._handle_vern(template)

        elif template_name == 'only used in':
            text, only = self._handle_only_used_in(template)
            self.only.extend(only)

        elif template_name == 'a':
            text, accents = self._handle_a(template)
            self.accents.extend(accents)

        elif template_name == 'fr-post-1990':
            text, post_1990 = self._handle_1990(template)
            self.post_1990 = post_1990

        elif template_name in ['gloss', 'gl', 'g']:
            text, glosses = self._handle_gloss(template)
            self.glosses.extend(glosses)

        elif template_name in ['qualifier', 'qual', 'qf', 'q', 'i']:
            text, qualifiers = self._handle_qualifier(template)
            self.qualifiers.extend(qualifiers)

        elif template_name in ['defdate', 'def-date']:
            text = self._handle_defdate(template, template_name)

        elif template_name in ['label', 'lb', 'tlb']:
            text, labels = self._handle_label(template)
            self.labels.extend(labels)

        elif template_name == 'topics':
            text, topics = self._handle_topics(template)
            self.topics.extend(topics)

        elif template_name == '1':
            text = self._handle_1(template)

        elif template_name == 'form of':
            text, form = self._handle_form_of(template)
            self.lemma = form

        elif template_name == 'fr-IPA':
            text = ''

        elif template_name == 'frac':
            text = self._handle_frac(template)

        elif template_name == 'senseid':
            text = self._handle_senseid(template)

        else:
            # None found, something is wrong
            print('Unknown template: ' + template)
            text = '<!--None-->'

        # Replace the template with the 'text'
        template_regex = re.escape(template)
        definition = re.sub(template_regex, text, definition)

        return definition

    def _handle_morphology(self, template, name):
        """
        Handles definition containing morphological variants. Replaces the main text with '@morphology' to indicate
        that it is not a lemma. Information contained in the original text is extracted and returned separately.
        :param template: template to be handled
        :param name: name of the template, e.g. 'inflection of'
        :return: tuple, '@morphology', lemma (original lemma to which the entry points), name (same as the argument),
        args (list of arguments template takes, e.g. '1', 'sg.'...
        """

        # Strip the template to get the lemma
        lemma = re.sub(r'{{' + name + r'\|(' + self.code + r'\|)?', '', template)
        lemma = re.sub(r'\|.*}}', '', lemma)
        lemma = re.sub(r'}}$', '', lemma)

        # Extract the arguments
        args = []
        args_found = re.search(r'\|\|.*}}', template)
        if args_found:
            args_str = args_found.group()
            args_str = re.sub(r'(\|\||}})', '', args_str)
            args.extend(args_str.split('|'))

        return '@morphology', lemma, name, args

    def _handle_variations(self, template):
        """
        Handles templates representing alternative forms for a lemma.
        :param template: templates for alternative forms
        :return: '@variation', word (lemma form)
        """

        # Prefixes
        word = re.sub(r'\|' + self.code, '', template)  # e.g. |fr
        word = re.sub(r'lang=\w{2,3}', '', word)        # e.g. lang=fr
        word = re.sub(r'nocap=\d', '', word)            # e.g. nocap=1

        # Suffixes
        word = re.sub(r'nodot=[\w\d]', '', word)        # e.g. nodot=1
        word = re.sub(r'gloss=[\w\s,\-]*', '', word)    # e.g. gloss=...
        word = re.sub(r'sort=[\w\s]*', '', word)        # e.g. sort=...
        word = re.sub(r'nocap=[\w\d]', '', word)        # e.g. nocap=1
        word = re.sub(r'\|dept', '', word)              # e.g. |dept
        word = re.sub(r'tr=[\w\s]*', '', word)          # e.g. tr=...
        word = re.sub(r'dot=[:;,]', '', word)           # e.g. dot=...
        word = re.sub(r't=[\w\s;]*]', '', word)         # e.g. t=...

        # todo: multiple choices must be dealt with
        word = re.sub(r'\|lang=\w*\|\|', ', ', word)    # e.g. ...|lang=fr||...
        word = re.sub(r'\|\|', '/', word)               # e.g. tuba basse||bass tuba
        word = re.sub(r'\|$', '', word)                 # e.g. asteur|
        word = re.sub(r'\|', '/', word)                 # e.g. maîtres|Maîtres
        return '@variation', word

    @staticmethod
    def _handle_form_of(template):
        """
        Handles 'form of' templates. It replaces the text with '@variation' and returns the lemma
        :param template: form of templates
        :return: '@variation', lemma
        """

        content = re.sub(r'{{form of\|\w{2,3}\|', '', template)
        content = re.sub(r'}}', '', content)
        contents = content.split('|')

        name = contents[0]  # fixme: name not used
        word = contents[1]

        return '@variation', word

    @staticmethod
    def _handle_strip(template, name):
        """
        Generically handles templates that simply need to be stripped.
        :param template: templates that need to be stripped
        :param name: name of the template
        :return: tuple, stripped text, glosses[] (list of glosses that are found as arguments for the template)
        """
        word = re.sub(r'{{' + re.escape(name) + r'\|(\w\w\|)?', '', template)

        # Prefixes
        word = re.sub(r'mul\|', '', word)  # e.g. mul|
        word = re.sub(r'cmn\|', '', word)  # e.g. cmn|

        # Suffixes
        word = re.sub(r'\|id=\w*', '', word)  # e.g.id=shah
        word = re.sub(r'\|ditr=\d', '', word)  # e.g. ditr=1
        word = re.sub(r'\|direct=\d', '', word)  # e.g. |direct=1
        word = re.sub(r'\|intr=\d', '', word)  # e.g. |intr=1
        word = re.sub(r'\|sc=Latn', '', word)  # e.g. |sc=Latn
        word = re.sub(r'\|;\|', '|', word)  # e.g. |;|

        # Extract and remove glosses
        glosses = re.findall(r'(?<=gloss(?:=|\|)).*(?=}})', word)  # e.g. gloss|... gloss=...
        word = re.sub(r'\|gloss(?:=|\|).*(?=}})', '', word)

        # Find translation (or explanation) and put them in a pair of parentheses
        translation_found = re.search(r'(\|\||\|t(r)?=).*(?=(}}|\|))', word)  # e.g. ||...}} or |t=...
        if translation_found:
            translation = translation_found.group()
            translation = ' (' + re.sub(r'(\|\||\|t(r)?=)', '', translation) + ')'
            word = re.sub(r'(\|\||\|t(r)?=).*(?=(}}|\|))', translation, word)

        # Extract additional information and put them in a pair of parentheses
        infos = re.findall(r'\|?\.\w+?(?=(?:}}|\|))', word)  # e.g. .pronominal
        for info in infos:
            replacement = ' (' + re.sub(r'\|\.', '', info) + ')'
            info = re.escape(info)
            word = re.sub(info, replacement, word)

        # Find qualifiers
        qualifiers = re.findall(r'\|qual\d=[^|]*(?=(?:}}|\|))', word)  # e.g. |qual1=...
        for qualifier in qualifiers:
            replacement = ' (' + re.sub(r'\|qual\d=', '', qualifier) + ')'
            qualifier = re.escape(qualifier)
            word = re.sub(qualifier, replacement, word)

        # Remove | at the beginning or the end
        word = re.sub(r'(^\s*\||\|\s*$)', '', word)

        # Replace | with / for alternatives
        word = re.sub(r'\|', '/', word)

        # Remove the last closing }}
        word = re.sub(r'}}', '', word)

        return word, glosses

    @staticmethod
    def _handle_lit(template):
        """
        Handles &lit template. Strip and replace the template with a description
        :param template:
        :return: replaced text
        """

        content = re.sub(r'{{&lit\|\w{2,3}\|', '', template)  # e.g. {{&lit|fr|...
        content = re.sub(r'\|dot=&nbsp;', ' ', content)  # |dot=&nbsp;
        content = re.sub(r'}}', '', content)
        content = re.sub(r'\|', ', ', content)  # multiple entries separated by |

        text = 'Used other than with a figurative or idiomatic meaning: see ' + content

        return text

    @staticmethod
    def _handle_ipa(template):
        """
        Strip IPAchar.
        :param template: IPAchar template
        :return: template content
        """

        text = re.sub(r'({{IPAchar\||}})', '', template)
        return text

    def _handle_latn_def(self, template):
        """
        Handle latin alphabet definition. There are two types: one is treated as 'Letter' and comes with integer
        to indicate the number in the alphabetical order, the other is treated as a noun and has names, e.g. A a
        :param template: Latn template
        :return: Expanded text to describe the letter
        """

        letter_found = re.search(r'{{Latn-def\|\w{2,3}\|letter\|\d+', template)

        # The entry is |letter|
        if letter_found:
            num = re.search(r'\d+', template).group()

            # Add correct suffixes
            if num.endswith('1'):
                num += 'st'
            elif num.endswith('2'):
                num += 'nd'
            elif num.endswith('3'):
                num += 'rd'
            else:
                num += 'th'

            text = 'The ' + num + ' letter of the ' + self.language + ' alphabet'

            # Alternative name
            alt_name_found = re.search(r'\|\D+}}', template)
            if alt_name_found:
                alt_name = alt_name_found.group()
                alt_name = re.sub(r'(\||}})', '', alt_name)
                text += ' called \'\'\'' + alt_name + '\'\'\' and written in the Latin script.'

        # The entry is |name|
        else:
            # Get the list of names, e.g. ['A', 'a']
            names = re.sub(r'{{Latn-def\|\w{2,3}\|name\|', '', template)
            names = re.sub(r'(}}|\|nodot=.)', '', names)
            names = names.split('|')

            text = 'The name of the Latin-script letter '
            for name in names:
                text += name + '/'
            text = text[:-1]
        return text

    @staticmethod
    def _handle_vern(template):
        """
        Handle vern template. Strip it to get the content and prefix with (vernacular) to indicate it is a vernacular
        name for a given concept
        :param template: vern template
        :return: (vernacular) word
        """

        word = re.sub(r'({{vern\||(\|pedia=\d)?}})', '', template)  # optional |pedia=1
        return '(vernacular) ' + word

    @staticmethod
    def _handle_only_used_in(template):
        """
        Handle 'only used in' template. These references other entries, they are return as the second element
        :param template: only used in templates
        :return: tuple, text (put translations in parenthesis if exists), words[] (list of words referenced)
        """

        # Remove
        word = re.sub(r'{{only used in\|\w{2,3}\|', '', template)
        word = re.sub(r'}}', '', word)

        # Find translation (or explanation) and put them in a pair of parentheses
        translation_found = re.search(r'\|\|.*', word)  # e.g. ||...}}
        translation = ''
        if translation_found:
            translation = translation_found.group()
            word = re.sub(r'\|\.*', '', word)
            translation = ' (' + re.sub(r'\|\|', '', translation) + ')'

        # Remove the last |
        word = re.sub(r'\|$', '', word)

        # Split into a list
        words = word.split('|term2=')

        # Text to replace
        text = word + translation

        return text, words

    @staticmethod
    def _handle_a(template):
        """
        Handles accent templates. Strip and put the content in parentheses (accent: ...). Return the accent tags as a
        list.
        :param template:
        :return:
        """

        # Strip
        accents = re.sub(r'{{a\|', '', template)
        accents = re.sub(r'}}', '', accents)

        # List of accents
        accents = accents.split('|')

        text = ' (accent: '
        for accent in accents:
            text += accent + ', '
        text = text[:-2] + ')'

        return text, accents

    @staticmethod
    def _handle_1990(template):
        """
        Handle the post-1990 template. Simply replace the template with the text (Post 1990 spelling) to indicate it
        is the modern French spelling. Return the content separately as well
        :param template: fr-post-1990 template
        :return: (Post 1990 spelling), fr-post-1990
        """

        word = re.sub(r'({{fr-post-1990\||}})', '', template)
        text = word + ' (Post 1990 spelling)'
        return text, word

    @staticmethod
    def _handle_gloss(template):
        """
        Handle the glosses. Strip the gloss and put the content in parenthesis. Also provide the gloss separately.
        :param template: gloss template
        :return: (gloss text), glosses (list of glosses)
        """

        glosses = re.sub(r'{{(gloss|gl|g)\|', '', template)
        glosses = re.sub(r'}}', '', glosses)

        return '(' + glosses + ')', glosses

    @staticmethod
    def _handle_qualifier(template):
        """
        Handle the qualifiers. Put the qualifiers in the original text in the format (qualifier1, qualifier2...)
        :param template: qualifier template
        :return: (qualifier text), qualifiers (list of qualifiers)
        """

        # Strip and separate
        qualifiers = re.sub(r'{{(qualifier|qual|qf|q|i)\|', '', template)
        qualifiers = re.sub(r'}}', '', qualifiers)
        qualifiers = qualifiers.split('|')

        # Concatenate the qualifiers
        text = '('
        for qualifier in qualifiers:
            text += qualifier + ', '

        text = text[:-2] + ')'  # remove the last comma
        return text, qualifiers

    @staticmethod
    def _handle_defdate(template, name):
        """
        Handles defdate template. Strip the template and put them in parentheses.
        :param template: defdate template
        :param name: 'defdate' or 'def-date'
        :return: stripped text in parentheses
        """

        text = re.sub(r'{{' + name + r'\|', '', template)
        text = ' (' + re.sub('}}', '', text) + ')'

        return text

    def _handle_label(self, template):
        """
        Handle label templates. Strip and split. Remove the original and return labels as lists
        :param template: label template
        :return: tuple, ('', labels[])
        """

        labels = re.sub(r'{{(label|lb|tlb)\|' + self.code + r'\|', '', template)
        labels = re.sub(r'\|_\|', ' ', labels)      # e.g. Western|_|Christianity
        labels = re.sub(r'(|}}|\|$)', '', labels)   # final }} and |

        labels = labels.split('|')
        return '', labels

    @staticmethod
    def _handle_topics(template):
        """
        Strip and separately provide topics
        :param template: topics template
        :return: tuple, ('', topics[])
        """

        topics = re.sub(r'{{topics\|\w{2,3}\|', '', template)   # \w{2,3} for language codes
        topics = re.sub(r'}}', '', topics)
        topics = topics.split('|')

        return '', topics

    def _handle_1(self, template):
        # todo:
        return '1-todo'

    @staticmethod
    def _handle_frac(template):
        """
        Handle fractions
        :param template: fraction template, {frac|a|b}
        :return:
        """

        frac = re.sub(r'{{frac\|', '', template)
        frac = re.sub(r'}}', '', frac)
        frac = re.sub(r'\|', '/', frac)     # e.g. 1/2

        return frac

    @staticmethod
    def _handle_senseid(template):
        """
        Handles senseid templates. Remove ids (e.g. Q....). Otherwise strip and return the result
        :param template: senseid template
        :return: stripped text
        """

        id_found = re.search(r'Q\d*', template)
        if id_found:
            word = ''
        else:
            word = re.sub(r'{{senseid\|\w{2,3}\|', '', template)  # {{senseid|language code (2-3 letters)|...
            word = ' (' + re.sub(r'}}', '', word) + ')'
        return word

    @staticmethod
    def _get_name(template):
        """
        Strip the template to get the name of the template
        :param template: template to be stripped
        :return: name of the template
        """

        name = re.sub(r'\|.*}}', '', template)
        name = re.sub(r'{{', '', name)
        return name

    @staticmethod
    def _strip_links(definition):
        """
        Removes any links
        :param definition: a line of definition
        :return: definition without links, i.e. [[.*]]
        """

        definition = re.sub(r'(#en\]\])', '', definition)         # e.g. [[protologue#en]]
        definition = re.sub(r'\[\[File:.*?\]\]', '', definition)  # e.g. [[File:...]]
        definition = re.sub(r'http(s)?:[^\s]*', '', definition)   # any hyperlinks, http:...
        definition = re.sub(r'\[\[[^\[\]]*?\|', '', definition)   # e.g. [[#English|...
        definition = re.sub(r'(\[|\])', '', definition)           # all [ and ]
        return definition




if __name__ == '__main__':
    # Todo: clean here
    LANGUAGE = 'Russian'
    handler = TemplateHandler(LANGUAGE)

    with open('heading_separated/' + LANGUAGE + '_heading_separated.json', 'r') as f:
        text = f.read()

    root = json.loads(text)

    definitions = []
    for entry in root['contents']:
        content = entry['content']
        content = remove_xml_tags(content)

        definitions.extend(re.findall(r'#\s.*', content))

    for definition in definitions:

        d = handler.handle_templates(definition)
        if '@' not in d['definition']:
            print(d['definition'])

    text = ''
    #for t in handler.debug_ipa:
    #    text += t + '\n'

    with open('ipa.txt', 'w') as f:
        f.write(text)
