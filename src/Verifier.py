import json
import re


class Verifier:
    """
    Verifier verifies if the definitions generated conform to the required format or not. It collects all the
    instances of errors (clearly wrong) and warnings (possibly wrong) cases and store in lists as tuples.
    """

    def __init__(self, json_path):
        """
        Initialize. Load JSON file of the data to be verified.
        :param json_path:
        """

        # Load JSON
        with open(json_path, 'r') as f:
            json_file = f.read()
        self.root = json.loads(json_file)

        # Lists of errors and warnings
        self.errors = []
        self.warnings = []

        # Sets of errors and warnings for types
        self.error_types = set()
        self.warning_types = set()

    def verify(self, errors=False, warnings=False):
        """
        Verify all of the definitions to see if they conform to the format.
        :param errors: print all errors if true
        :param warnings: print all warnings if true
        """

        # Verify each components
        self._verify_definitions()
        self._verify_lemmas()

        # Print results
        print('{0:d} errors found, {1:d} types'.format(len(self.errors), len(self.error_types)))
        for t in self.error_types:
            print('\t type: ' + t)
        print('{0:d} warnings found, {1:d} types'.format(len(self.warnings), len(self.warning_types)))
        for t in self.warning_types:
            print('\t type: ' + t)

        # Print all else
        if errors:
            for error in self.errors:
                print(error[0])
                print(error[1] + '\n')
        if warnings:
            for warning in self.warnings:
                print(warning[0])
                print(warning[1] + '\n')

    def _add_error(self, definition, err_message):
        """
        Add a new error
        :param definition: definition
        :param err_message: error message
        """

        self.errors.append((definition, err_message))
        self.error_types.add(err_message)

    def _add_warning(self, definition, war_message):
        """
        Add a new warning
        :param definition: definition
        :param war_message: warning message
        """

        self.warnings.append((definition, war_message))
        self.warning_types.add(war_message)

    def _verify_definitions(self):
        for entry in self.root['entries']:
            for definition in entry['definitions']:
                def_text = definition['definition']

                if re.search(r'\s', def_text):
                    self._verify_space(def_text)

                if re.search(r'[\"\'’“”«»]', def_text):
                    self._verify_quotation_marks(def_text)

                if re.search(r'[!?,.:;\-§†…]', def_text):
                    self._verify_punctuations(def_text)

                if re.search(r'[(){}\[\]⟨⟩]', def_text):
                    self._verify_parentheses(def_text)

                if re.search(r'@', def_text):
                    self._verify_at(def_text)

                if re.search(r'&', def_text):
                    self._verify_amp(def_text)

                if re.search(r'#', def_text):
                    self._verify_sharp(def_text)

                if re.search(r'\^', def_text):
                    self._verify_caret(def_text)

                if re.search(r'¨', def_text):
                    self._verify_diaeresis(def_text)

                if re.search(r'[|\\/]', def_text):
                    self._verify_separators(def_text)

                if re.search(r'(<.*?>.*?</.*?>|<.*?/>|<!--.*?-->)', def_text):
                    self._verify_xml(def_text)

                if re.search(r'[+=<>~∞≈≤≥⩽⩾]', def_text):
                    self._verify_math_symbols(def_text)

                if re.search(r'[–—¶‽⁄Ⓚ♠]', def_text):
                    self._verify_misc(def_text)

                if re.search(r'[^\w\s\"\'’“”«»!?,.:;\-§†…(){}\[\]⟨⟩@&#^¨|\\/<>+=~∞≈≤≥⩽⩾–—¶‽⁄Ⓚ♠]', def_text):
                    self._add_error(def_text, 'Unknown symbol found')

    def _verify_lemmas(self):
        for entry in self.root['entries']:
            for definition in entry['definitions']:
                lemma = definition['lemma']

                if re.search('[{}]', lemma):
                    self._add_error(lemma, 'Curly brackets are not allowed')
                if re.search('[\[\]], lemma', lemma):
                    self._add_error(lemma, 'Square brackets are not allowed')

    def _verify_space(self, definition):
        """
        Check for leading/trailing spaces and multiple spaces
        :param definition: definition
        """

        if re.search(r'(^\s|\s$)', definition):
            self._add_error(definition, 'Leading/trailing spaces are not allowed')
        if re.search(r'\s{2,}', definition):
            self._add_error(definition, 'Multiple spaces are not allowed')

    def _verify_quotation_marks(self, definition):
        """
        Check quotations ", ', ’, “, ”, «, ». Issue warning if ", “, ”, «, », are not properly paired
        :param definition: definition
        """

        # Look for odd number of ", “ ”, « »
        if len(re.findall(r'\"', definition)) % 2 is not 0:
            self._add_warning(definition, '"s may not match up')
        if len(re.findall(r'[“”]', definition)) % 2 is not 0:
            self._add_warning(definition, '“ and ” may not match up')
        if len(re.findall('[«»]', definition)) % 2 is not 0:
            self._add_warning(definition, '« and» may not match up')

    def _verify_punctuations(self, definition):
        """
        Check punctuations !, ?, ,, ., :, ;, -, §, †, …. Issue warning if §, † are found.
        :param definition: definition
        """

        if re.search(r'[§†]', definition):
            self._add_warning(definition, '§, † are rare')

    def _verify_parentheses(self, definition):
        """
        Check parentheses (), {}, [], ⟨⟩. Throw an error is {} or [] are found. Issue warning if (), ⟨⟩ don't match up.
        :param definition: definition
        """

        if re.search(r'[{}]', definition):
            self._add_error(definition, 'Curly brackets are not allowed')
        if re.search(r'[\[\]]', definition):
            self._add_error(definition, 'Square brackets are not allowed')

        if len(re.findall('[()]', definition)) % 2 is not 0:
            self._add_warning(definition, '() may not match up')
        if len(re.findall('[⟨⟩]', definition)) % 2 is not 0:
            self._add_warning(definition, '⟨⟩ may not match up')

    def _verify_at(self, definition):
        """
        Check @ marks. If it is not recognized issue a warning.
        :param definition: definition
        """

        if re.search(r'@(?!(morphology|variation))', definition):
            self._add_warning(definition, 'Unrecognized use of at mark')

    def _verify_amp(self, definition):
        """
        Check &. Thrown an error is it is a part of HTML code, else issue a warning.
        :param definition: definition
        """

        if re.search(r'&.*?;', definition):
            self._add_error(definition, 'HTML code not allowed')
        else:
            self._add_warning(definition, '& allowed only in limited cases')

    def _verify_sharp(self, definition):
        """
        Throw an error for #.
        :param definition: definition
        """

        self._add_warning(definition, '# allowed only in limited cases')

    def _verify_caret(self, definition):
        """
        Issue a warning for ^.
        :param definition: definition
        """

        self._add_warning(definition, '^ allowed only in limited cases')

    def _verify_diaeresis(self, definition):
        """
        Issue a warning for a diaeresis
        :param definition:
        """

        self._add_warning(definition, '¨ allowed only in limited cases')

    def _verify_separators(self, definition):
        """
        Check separators |, \, /. If | is found throw and error. Issue warning for \.
        :param definition:
        """

        if re.search(r'\|', definition):
            self._add_error(definition, '| are not allowed')
        if re.search(r'\\', definition):
            self._add_warning(definition, '\\ allowed only in limited cases')

    def _verify_xml(self, definition):
        """
        Check XML tags for validity. Only <sub>, <sup> and <math> are allowed.
        :param definition: definition
        """

        allowed = ['sub', 'sup', 'math']

        # Strip XML tag and check
        if re.search(r'<.*?>.*?</.*?>', definition):
            name = re.search(r'</.*?>', definition).group()
            name = re.sub(r'(</|>)', '', name)

            if name not in allowed:
                self._add_error(definition, 'XML tag \"' + name + '\" is not allowed')
        # Strip inline XML tag and check
        if re.search(r'<.*?/>', definition):
            name = re.search(r'<.*?/>', definition).group()
            name = re.sub(r'(<|/>)', '', name)

            if name not in allowed:
                self._add_error(definition, 'XML tag \"' + name + '\" is not allowed')

        # XML comments
        if re.search(r'<!--.*?-->', definition):
            self._add_error(definition, 'XML comments are not allowed')

    def _verify_math_symbols(self, definition):
        """
        Check math symbols +, =, <, >, ~, ∞, ≈, ≤, ≥, ⩽, ⩾ and issue warning.
        :param definition: definition
        """

        if re.search(r'=', definition):
            self._add_error(definition, '= are not allowed')
        self._add_warning(definition, 'Math symbols are rare')

    def _verify_misc(self, definition):
        """
        Check special characters –, —, ¶, ‽, ⁄, Ⓚ, ♠ and issue warning
        :param definition: definition
        """

        self._add_warning(definition, 'Very rare miscellaneous symbols')


if __name__ == '__main__':
    # todo: clean here
    LANGUAGE = 'French'
    verifier = Verifier('../data/formatted/' + LANGUAGE + '_formatted.json')

    verifier.verify(errors=False, warnings=False)


